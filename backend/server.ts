import { Request, Response, NextFunction } from "express";
import http from "http";
import express from "express";
import dotenv from "dotenv";
import { Server } from "socket.io";

dotenv.config();

const PORT: number | string = process.env.PORT || 8080;

const app = express();

app.use((req: Request, res: Response, next: NextFunction) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  next();
});

const httpServer = http.createServer(app);

const io = new Server(httpServer, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
});

httpServer.listen(PORT, () => {
  console.log(`available on *:${PORT}`);
});

io.on("connection", (socket: any) => {
  socket.emit("connection", null);

  socket.emit("currentUser", socket.id);

  socket.on("disconnect", () => {
    socket.broadcast.emit("callEnded");
  });

  socket.on("shutdown", () => {
    io.emit("shutdownCall");
  });

  socket.on(
    "callUser",
    (data: { userToCall: string; signalData: any; from: string }) => {
      io.to(data.userToCall).emit("callUser", {
        userToCall: data?.userToCall,
        signal: data.signalData,
        from: data.from,
      });
    }
  );

  socket.on("answerCall", (data: any) => {
    io.to(data.to).emit("callAccepted", data.signal);
  });

  socket.on("send-pos", (pos: { x: number; y: number }) => {
    io.emit("drag", pos);
  });

  socket.on("send-scale", (scale: number) => {
    io.emit("scale", scale);
  });
});
