import React, { FC, Fragment, useEffect, useState } from "react";
import { io } from "socket.io-client";
import { Routes, Route } from "react-router-dom";
import "./App.scss";

import { SERVER } from "./constant";
import Owner from "./pages/Owner";
import Client from "./pages/Client";
import Loading from "./components/Loading";

const socket = io(SERVER);

const App: FC = () => {
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    socket.connect();
  }, []);

  return (
    <Fragment>
      {loading && <Loading />}
      <div className="wrapper">
        <div className="container">
          <Routes>
            <Route path="/owner" element={<Owner socket={socket} />} />
            <Route
              path="/client/:ownerId"
              element={<Client socket={socket} setLoading={setLoading} />}
            />
          </Routes>
        </div>
      </div>
    </Fragment>
  );
};

export default App;
