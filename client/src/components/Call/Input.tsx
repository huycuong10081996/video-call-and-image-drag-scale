import React, {
  FC,
  Fragment,
  CSSProperties,
  ReactNode,
  ChangeEvent,
} from "react";

export interface IInputedComponent {
  label: string | undefined;
  style?: CSSProperties;
  name: string | undefined;
  children?: ReactNode;
  className?: string | undefined;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  value: string | undefined;
  disabled?: boolean;
}

const Input: FC<IInputedComponent> = ({
  label,
  name,
  value,
  onChange,
  ...props
}: IInputedComponent) => {
  return (
    <Fragment>
      <label htmlFor={name}>{label}</label>
      <input name={name} value={value} onChange={onChange} {...props} />
    </Fragment>
  );
};

export default Input;
