import React, { useEffect, useState, FC, RefObject, Fragment } from "react";
import Peer from "simple-peer";
import { Socket } from "socket.io-client";

import "./style.scss";
import Input from "./Input";
import Dotpluse from "./DotPluse";
import { BASE_URL } from "../../constant";

export interface ICallComponent {
  socket: Socket;
  videoCallRef: RefObject<HTMLVideoElement>;
  userVideo: RefObject<HTMLVideoElement>;
  connectionRef: any;
  isOwner?: boolean;
  ownerId?: string;
  setLoading?: (loading: boolean) => void | undefined;
}

const Call: FC<ICallComponent> = ({
  socket,
  videoCallRef,
  userVideo,
  connectionRef,
  isOwner,
  ownerId,
  setLoading,
}) => {
  const [currentUser, setCurrentUser] = useState("");
  const [stream, setStream] = useState<any | null>(null);
  const [receivingCall, setReceivingCall] = useState(false);
  const [caller, setCaller] = useState("");
  const [signal, setSignal] = useState<any | null>(null);
  const [callAccepted, setCallAccepted] = useState(false);
  const [callEnded, setCallEnded] = useState(false);
  const [clientURL, setClientURL] = useState("");

  useEffect(() => {
    navigator.mediaDevices
      .getUserMedia({ video: true, audio: true })
      .then((stream) => {
        setStream(stream);
        if (videoCallRef.current) videoCallRef.current.srcObject = stream;
      });

    socket.on("currentUser", (id: string) => {
      setCurrentUser(id);
    });

    socket.on(
      "callUser",
      (data: { from: React.SetStateAction<string>; signal: any }) => {
        setReceivingCall(true);
        setCaller(data.from);
        setSignal(data.signal);
      }
    );

    socket.on("shutdownCall", () => {
      setCallEnded(true);
      connectionRef.current.destroy();
      window.location.reload();
    });

    return () => {
      socket.emit("disconnect");
    };
  }, []);

  useEffect(() => {
    typeof setLoading === "function" && setLoading(true);

    if (ownerId && currentUser) {
      typeof setLoading === "function" && setLoading(false);
      callUser(ownerId);
    }
  }, [ownerId, currentUser]);

  const callUser = (id: string) => {
    if (!id) {
      window.alert("Please enter a user id");
    }

    const simplePeer = new Peer({
      initiator: true,
      trickle: false,
      stream,
    });

    simplePeer.on("signal", (data: any) => {
      socket.emit("callUser", {
        userToCall: id,
        signalData: data,
        from: currentUser,
      });
    });

    simplePeer.on("error", (err) => {
      console.log(err);
      window.alert("The client URL is expired!");
    });

    simplePeer.on("stream", (stream: MediaStream) => {
      if (userVideo.current) userVideo.current.srcObject = stream;
    });

    socket.on("callEnded", () => {});

    socket.on("callAccepted", (signal: any) => {
      setCallAccepted(true);
      simplePeer.signal(signal);
    });

    connectionRef.current = simplePeer;
  };

  const answer = () => {
    setCallAccepted(true);
    const simplePeer = new Peer({
      initiator: false,
      trickle: false,
      stream,
    });

    simplePeer.on("signal", (data: any) => {
      socket.emit("answerCall", { signal: data, to: caller });
    });

    simplePeer.on("stream", (stream: MediaStream) => {
      if (userVideo.current) userVideo.current.srcObject = stream;
    });

    simplePeer.signal(signal);
    connectionRef.current = simplePeer;
  };

  const shutdown = () => {
    setCallEnded(true);
    socket.emit("shutdown");
  };

  const rendererCallBtn = () => (
    <div className="call-button">
      {callAccepted && !callEnded && (
        <button className="btn-call" onClick={shutdown}>
          End Call
        </button>
      )}
    </div>
  );

  const rendererAnswerBtn = () => (
    <div style={{ width: "100%" }}>
      {receivingCall && !callAccepted && (
        <div className="caller">
          <h3 className="color-base">
            {caller} is calling <Dotpluse />
          </h3>
          <button
            className="btn-call"
            style={{ width: "331.667px" }}
            onClick={answer}
          >
            Answer
          </button>
        </div>
      )}
    </div>
  );

  const handleGetClientURL = () => {
    setClientURL(`${BASE_URL}/client/${currentUser}`);
  };

  const renderOwnerSpace = () => {
    return (
      <Fragment>
        <Input
          label="Client url"
          name="idToCall"
          value={clientURL}
          disabled={true}
          className="padding-base"
        />
        <div className="call-button">
          <button className="btn-call" onClick={handleGetClientURL}>
            Get client URL
          </button>
        </div>
      </Fragment>
    );
  };

  return (
    <Fragment>
      <h1>Video Call</h1>

      <div className="call-container">
        <div className="video-container">
          <div className="video">
            {stream && (
              <video
                playsInline
                muted
                ref={videoCallRef}
                autoPlay
                className="width-300"
              />
            )}
          </div>
          <div className="video">
            {callAccepted && !callEnded && (
              <video
                playsInline
                ref={userVideo}
                autoPlay
                className="width-300"
              />
            )}
          </div>
        </div>
        <div className="myId">
          {isOwner && renderOwnerSpace()}
          {!isOwner && (
            <div className="padding-base">Your id: {currentUser}</div>
          )}
          {rendererCallBtn()}
        </div>
        {rendererAnswerBtn()}
      </div>
    </Fragment>
  );
};

export default Call;
