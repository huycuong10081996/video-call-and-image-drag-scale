import React, { FC } from "react";
import "./style.scss";

const Loading: FC = () => {
  return (
    <div className="loading-wrapper">
      <div className="kinetic"></div>
    </div>
  );
};
export default Loading;
