import React, { FC, Fragment, useEffect } from "react";
import { Rect, Stage, Layer } from "react-konva";
import { Socket } from "socket.io-client";

export interface IDragComponent {
  socket: Socket;
  dragRef: any;
}

const Reactangle: FC<IDragComponent> = ({ socket, dragRef }) => {
  useEffect(() => {
    socket.on("drag", (pos: { x: number; y: number }) => {
      dragRef.current.to({
        duration: 0.2,
        x: pos?.x,
        y: pos?.y,
      });
    });
  }, []);

  const changeSize = () => {
    const position: { x: number; y: number } =
      dragRef.current.getAbsolutePosition();
    socket.emit("send-pos", position);
  };

  return (
    <Fragment>
      <h1>Drag</h1>

      <Stage width={window.innerWidth} height={200}>
        <Layer>
          <Rect
            ref={dragRef}
            width={50}
            height={50}
            fill="green"
            draggable
            onDragEnd={changeSize}
            onDragStart={changeSize}
          />
        </Layer>
      </Stage>
    </Fragment>
  );
};

export default Reactangle;
