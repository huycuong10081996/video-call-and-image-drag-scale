import React, { useEffect, useState, FC, Fragment, ChangeEvent } from "react";
import { Socket } from "socket.io-client";
import { Rect, Stage, Layer } from "react-konva";

export interface IScaleComponent {
  socket: Socket;
  scaleRef: any;
}

const ReactangleScale: FC<IScaleComponent> = ({ socket, scaleRef }) => {
  const [scale, setScale] = useState(1);
  useEffect(() => {
    socket.on("scale", (scale: number) => {
      setScale(scale);
      scaleRef.current.to({
        scaleX: +scale,
        scaleY: +scale,
        duration: 0.2,
      });
    });
  }, []);

  const handleInputScaleChange = (e: ChangeEvent<HTMLInputElement>) => {
    socket.emit("send-scale", e.target.value);
  };

  return (
    <Fragment>
      <h1>Scale Image</h1>
      <label htmlFor="scale">Value to scale &nbsp;</label>
      <input
        type="text"
        value={scale}
        name="scale"
        onChange={handleInputScaleChange}
      />
      <Stage width={window.innerWidth} height={200}>
        <Layer>
          <Rect ref={scaleRef} width={50} height={50} fill="green" />
        </Layer>
      </Stage>
    </Fragment>
  );
};

export default ReactangleScale;
