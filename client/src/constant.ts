const SERVER = process.env.REACT_APP_SOCKET_SERVER || "ws://localhost:8080";
const BASE_URL = process.env.REACT_APP_BASE_URL || "http://localhost:3000";

const isEnabledImageDrag = process.env.REACT_APP_IS_ENABLED_IMAGE_DRAG;
const isEnabledImageScale = process.env.REACT_APP_IS_ENABLED_IMAGE_SCALE;

export { SERVER, isEnabledImageDrag, isEnabledImageScale, BASE_URL };
