import React, { FC, Fragment, useRef } from "react";
import { Socket } from "socket.io-client";
import { useParams } from "react-router-dom";

import Call from "../../components/Call";
import RectangleDrag from "../../components/RectangleDrag";
import RectangleScale from "../../components/RectangleScale";
import { isEnabledImageDrag, isEnabledImageScale } from "../../constant";

interface IClientComponent {
  children?: React.ReactNode;
  socket: Socket;
  setLoading?: (loading: boolean) => void | undefined;
}

const Client: FC<IClientComponent> = ({ socket, setLoading }) => {
  const { ownerId } = useParams();
  const dragRef = useRef(null);
  const scaleRef = useRef(null);

  const videoCallRef = useRef<HTMLVideoElement>(null);
  const userVideo = useRef<HTMLVideoElement>(null);
  const connectionRef = useRef(null);

  return (
    <Fragment>
      <Call
        ownerId={ownerId}
        socket={socket}
        videoCallRef={videoCallRef}
        userVideo={userVideo}
        connectionRef={connectionRef}
        setLoading={setLoading}
      />
      <hr />

      {isEnabledImageDrag && (
        <Fragment>
          <RectangleDrag socket={socket} dragRef={dragRef} />
          <hr />
        </Fragment>
      )}

      {isEnabledImageScale && (
        <RectangleScale socket={socket} scaleRef={scaleRef} />
      )}
    </Fragment>
  );
};
export default Client;
