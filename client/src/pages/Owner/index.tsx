import React, { FC, Fragment, useRef } from "react";
import { Socket } from "socket.io-client";

import Call from "../../components/Call";
import RectangleDrag from "../../components/RectangleDrag";
import RectangleScale from "../../components/RectangleScale";
import { isEnabledImageDrag, isEnabledImageScale } from "../../constant";

interface IOwnerComponent {
  children?: React.ReactNode;
  socket: Socket;
}

const Owner: FC<IOwnerComponent> = ({ socket }) => {
  const dragRef = useRef(null);
  const scaleRef = useRef(null);

  const videoCallRef = useRef<HTMLVideoElement>(null);
  const userVideo = useRef<HTMLVideoElement>(null);
  const connectionRef = useRef(null);

  return (
    <Fragment>
      <Call
        isOwner={true}
        socket={socket}
        videoCallRef={videoCallRef}
        userVideo={userVideo}
        connectionRef={connectionRef}
      />
      <hr />

      {isEnabledImageDrag && (
        <Fragment>
          <RectangleDrag socket={socket} dragRef={dragRef} />
          <hr />
        </Fragment>
      )}

      {isEnabledImageScale && (
        <RectangleScale socket={socket} scaleRef={scaleRef} />
      )}
    </Fragment>
  );
};
export default Owner;
